﻿using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    private int currentStep;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        InitGame();
    }

    void InitGame()
    {
        StepHandlers = new List<IHasStepHandler>();
    }

    void Update()
    {

    }

    private ICollection<IHasStepHandler> StepHandlers = new List<IHasStepHandler>();

    public void AddStepHandler(IHasStepHandler handler)
    {
        StepHandlers.Add(handler);
    }

    public void BeforeMove()
        => StepHandlers.Apply(h => h.BeforeMove());

    public void AfterMove()
        => StepHandlers.Apply(h => h.AfterMove());

    public void Restart()
    {
        StepHandlers = new List<IHasStepHandler>();
        SceneManager.LoadScene(0);
    }
}