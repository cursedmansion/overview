﻿namespace Assets.Scripts
{
    public interface IMovingObject
    {
        float SpeedValue { get; }
    }
}