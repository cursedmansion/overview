﻿namespace Assets.Scripts
{
    public interface IHasStepHandler
    {
        void BeforeMove();

        void AfterMove();
    }
}
