﻿using System.Threading;
using Assets.Scripts;
using UnityEngine;

public class LineAttackEnemy : Enemy, 
    IHasStepHandler,
    IMovingObject
{
    [HideInInspector]
    public bool IsActivated = false;

    public Vector2 Direction;

    public int DelayInSteps;
    public float DelayInSecs;
    public float Speed;
    public LayerMask BlockingLayer;

    void Start()
    {
        if (DelayInSteps > 0)
        {
            GameManager.instance.AddStepHandler(this);
        }
    }


    void FixedUpdate()
    {
        if (!IsActivated)
            return;

        DelayInSecs-= Time.deltaTime;

        if (DelayInSecs > 0)
            return;

        if (DelayInSteps > 0)
            return;

        var destination = transform.position + Direction.As3D();

        var hit = Physics2D.Raycast(transform.position, Direction.As3D().normalized, Direction.magnitude, BlockingLayer);

        if (hit.collider != null)
            destination = hit.transform.position - Direction.As3D().normalized;

        IsActivated = false;

        SmoothMovementHelper.Move(this, destination,
            () => { },
            () =>
            {
                Destroy(this.gameObject, 1);
                Destroy(this);
            });
    }

    public void BeforeMove()
    {
        
    }

    public void AfterMove()
    {
        if (!IsActivated)
            return;

        DelayInSteps--;
    }

    public float SpeedValue => Speed;
}