﻿namespace Assets.Scripts
{
    public interface IHasTrigger
    {
        void OnTrigger();
    }
}