﻿using UnityEngine;

public class LineAttackEnemyTrigger : MonoBehaviour
{
    public LineAttackEnemy Enemy;
    
    public void OnTriggerEnter2D(Collider2D collider)
    {
        Enemy.IsActivated = true;
    }
}