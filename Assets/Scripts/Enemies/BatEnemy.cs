﻿using System.Collections.Generic;
using System.Threading;
using Assets.Scripts;
using UnityEngine;

public class BatEnemy : Enemy, IHasStepHandler, IMovingObject
{
    public LayerMask layerMask;
    public LayerMask BlockingLayer;
    public float speed = 5f;
    
    public int TriggerDistance;
    public int DelaySteps = 1;
    public int MovementDistance = 3;

    private ICollection<Vector3> _triggerDirections;
    private Vector3 _currentDirection;
    private int _currentDelay;

    private Animator _animator;
    
    public float SpeedValue => speed;

    void Start()
    {
        IsActive = false;
        _triggerDirections = new [] { transform.right, -transform.right, transform.up, -transform.up };
        _animator = GetComponentInChildren<Animator>();

        GameManager.instance.AddStepHandler(this);
    }

    Result<Vector3> CheckPlayer(Vector3 scanDirection)
    {
        var hit = Physics2D.Raycast(transform.position, scanDirection, TriggerDistance, layerMask);

        return hit.collider?.tag == Tags.Player 
            ? Result<Vector3>.Success(scanDirection) 
            : Result<Vector3>.Fail();
    }

    public void BeforeMove()
    {
    }

    public void AfterMove()
    {
        if (!IsActive)
        {
            WatchPlayer();
        }
        else
        {
            Attack();
        }
    }

    private void Attack()
    {
        if (_currentDelay == 0)
        {
            var destination = transform.position + _currentDirection * MovementDistance;

            var hit = Physics2D.Raycast(transform.position, _currentDirection, MovementDistance, BlockingLayer);

            if (hit.collider != null)
                destination = hit.transform.position - _currentDirection;

            SmoothMovementHelper.Move(this, destination,
                () =>
                {
                    _animator.SetBool("alert", false);
                },
                () =>
                {
                    _currentDelay = DelaySteps;

                    if (hit.collider != null)
                        IsActive = false;
                });
        }
        else
        {
            _currentDelay--;
        }
    }

    private void WatchPlayer()
    {
        foreach (var newDirection in _triggerDirections)
        {
            var result = CheckPlayer(newDirection);
            if (!result.IsSuccess)
                continue;

            _currentDirection = result.Value;
            _currentDelay = DelaySteps;
            _animator.SetBool("alert", true);

            IsActive = true;

            return;
        }
    }
}
