﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class SpitSpider : Enemy, IHasStepHandler
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    private readonly Dictionary<Direction, Vector3> _directionMap = new Dictionary<Direction, Vector3>()
    {
        {Direction.Up, Vector3.up},
        {Direction.Down, -Vector3.up},
        {Direction.Left, Vector3.left},
        {Direction.Right, -Vector3.left},
    };

    public Direction SpitDirection;

    public int TriggerDistance;

    public int SpitDistance;

    public float DelayInSec;

    public LayerMask TriggerLayerMask;
    public LayerMask BlockingLayerMask;

    public GameObject SpitObject;

    public float SpitSpeed = 3;

    private Animator _animator;
    
    private DateTime? _triggerTime;
    
    void Start()
    {
        _animator = GetComponentInChildren<Animator>();

        IsActive = false;

        GameManager.instance.AddStepHandler(this);
    }

    void FixedUpdate()
    {
        if (IsActive)
        {
            if (DateTime.Now.Subtract(_triggerTime.Value).TotalSeconds < DelayInSec)
                return;

            var spit = Instantiate(SpitObject, transform.position, Quaternion.identity);
            var enemy = spit.AddComponent<LineAttackEnemy>();
            enemy.IsActive = true;
            enemy.IsActivated = true;
            enemy.Direction = SpitDistance * _directionMap[SpitDirection];
            enemy.DelayInSecs = 0;
            enemy.DelayInSteps = 0;
            enemy.BlockingLayer = BlockingLayerMask;
            enemy.Speed = SpitSpeed;

            var spitCollider = spit.AddComponent<BoxCollider2D>();
            spitCollider.isTrigger = true;

			IsActive = false;
        }
    }

    public void BeforeMove()
    {
        
    }

    public void AfterMove()
    {
        if (!IsActive)
        {
            WatchPlayer();
        }
    }

    private void WatchPlayer()
    {
        var result = CheckPlayer(_directionMap[SpitDirection]);
        if (!result.IsSuccess)
            return;

        _triggerTime = DateTime.Now;

		_animator.SetTrigger("onAlert");

        IsActive = true;
    }

    Result<Vector3> CheckPlayer(Vector3 scanDirection)
    {
        var hit = Physics2D.Raycast(transform.position, scanDirection, TriggerDistance, TriggerLayerMask);

        return hit.collider?.tag == Tags.Player
            ? Result<Vector3>.Success(scanDirection)
            : Result<Vector3>.Fail();
    }
}