﻿using UnityEngine;
using UnityEngine.SceneManagement;


public abstract class Enemy : MonoBehaviour
{
    public bool IsActive = true;

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (!IsActive)
            return;

        if (other.tag == "Player")
        {
            GameManager.instance.Restart();
        }
    }
}