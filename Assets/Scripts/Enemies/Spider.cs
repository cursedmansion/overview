﻿using Assets.Scripts;
using UnityEngine;

public class Spider : Enemy, IHasStepHandler
{
    public int LiveCount = 1,
        SleepCount = 2;

    public int State = 0;

    private Animator _animator;

    void Start()
    {
        _animator = GetComponentInChildren<Animator>();

        SetState(State);
        
        GameManager.instance.AddStepHandler(this);
    }

    public void BeforeMove()
    {
        IncreaseState();
        SetState(State);
    }

    public void AfterMove()
    {
    }
    
    private void IncreaseState()
    {
        State++;

        if (State >= LiveCount + SleepCount)
            State = 0;
    }

    private void SetState(int state)
    {
        if (state < LiveCount)
        {
            _animator?.SetInteger("state", 3);
            IsActive = true;
        }
        else if (state == LiveCount)
        {
            _animator?.SetInteger("state", 1);
            IsActive = false;
        }
        else
        {
            _animator?.SetInteger("state", 2);
            IsActive = false;
        }
    }
}