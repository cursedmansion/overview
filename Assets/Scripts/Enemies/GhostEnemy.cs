﻿using System;
using Assets.Scripts;
using UnityEngine;

public class GhostEnemy : Enemy, IHasTrigger
{
    public float Speed = 1;

    public float DelayInSec = 0;
    private DateTime? _triggerTime;

	private Animator _animator;

    void Start()
    {
		_animator = GetComponentInChildren<Animator>();
    }

    void FixedUpdate()
    {
        if (!IsActive)
            return;

        if (DateTime.Now.Subtract(_triggerTime.Value).TotalSeconds < DelayInSec)
            return;

        var target = GameObject.FindGameObjectWithTag("Player").transform;

        var nextPosition = Vector3.MoveTowards(transform.position, target.position, Speed * Time.deltaTime);

        this.transform.position = nextPosition;
    }

    public void OnTrigger()
    {
        IsActive = true;
		_animator.SetTrigger("onAlert");
		_triggerTime = DateTime.Now;
    }
}