﻿using System;
using System.Threading;
using Assets.Scripts;
using UnityEngine;

public partial class Player : MonoBehaviour, IMovingObject
{
    public LayerMask blockingLayer;

    public float Speed = 3;

    private bool isMoving = false;
    private Animator _animator;
    private CancellationTokenSource _moveCancellationTokenSource;
    private Action _afterMoving;

    public float SpeedValue => Speed;

    void Start()
    {
        _animator = this.GetComponentInChildren<Animator>();
    }

    void FixedUpdate()
    {
        if (isMoving)
            return;
        
        var directionResult = GetDirectionFromInputOnce();

        if (directionResult.IsFailed)
            return;

        var direction = directionResult.Value;
        
        var start = transform.position;
        var end = start + direction.As3D();

        var item = Physics2D.Linecast(start, end, blockingLayer);
        if (item.transform)
        {
            return;
        }

        _moveCancellationTokenSource = new CancellationTokenSource();
        SmoothMovementHelper.Move(this, end, OnMoveStart, OnMoveEnd);
    }

    public void AfterMoving(Action action) 
        => _afterMoving = action;

    private void OnMoveStart()
    {
        GameManager.instance.BeforeMove();
        _animator.SetBool("isWalking", true);
        isMoving = true;
    }

    private void OnMoveEnd()
    {
        GameManager.instance.AfterMove();
        _animator.SetBool("isWalking", false);
        isMoving = false;

        if (_afterMoving != null)
        {
            _afterMoving();
            _afterMoving = null;
        }
    }

    private Result<Vector2> GetDirectionFromInputOnce()
    {
        var horizontal = (int)Input.GetAxisRaw("Horizontal");
        var vertical = (int)Input.GetAxisRaw("Vertical");

        Input.ResetInputAxes();

        if (horizontal != 0)
        {
            vertical = 0;
        }

        if (horizontal == 0 && vertical == 0)
            return Result<Vector2>.Fail();

        return Result<Vector2>.Success(new Vector2(horizontal, vertical).normalized);
    }
}