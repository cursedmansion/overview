﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;
	public Transform target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//transform.position = new Vector3 (target.transform.position.x, target.transform.position.y, transform.position.z);
		if (target)
		{
			//Vector3 point = target.position;
			//Vector3 delta = target.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			//Vector3 destination = transform.position + delta;
			Vector3 point = new Vector3(target.position.x, target.position.y, transform.position.z);
			transform.position = Vector3.SmoothDamp(transform.position, point, ref velocity, dampTime);
		}
	}
}
