﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using UnityEngine;

public class Spawner : TriggerBase
{
    public GameObject SpawnObject;
    public Vector3 SpawnPositionRelative;

    void Start()
    {
    }
    
    protected override void TriggerAction() 
        => Instantiate(SpawnObject, transform).transform.Translate(SpawnPositionRelative);
}