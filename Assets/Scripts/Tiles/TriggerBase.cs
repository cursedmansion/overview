﻿using UnityEngine;

public abstract class TriggerBase : MonoBehaviour
{
    public bool TriggerOnPlayer = true;
    public bool TriggerOnEnemy = false;
    public bool IsActive = true;
    public bool IsRepeatable = true;

    private bool _isTriggered = false;
  
    void OnTriggerEnter2D(Collider2D other)
        => ProcessTrigger(other);

    void OnTriggerStay2D(Collider2D other)
        => ProcessTrigger(other);

    void OnTriggerExit2D(Collider2D other)
        => _isTriggered = !IsRepeatable;

    private void ProcessTrigger(Collider2D other)
    {
        if (!IsActive)
            return;

        if (_isTriggered)
            return;

        if (other.gameObject.tag == Tags.Player && !TriggerOnPlayer)
            return;

        if (other.gameObject.tag == Tags.Enemy && !TriggerOnEnemy)
            return;

        _isTriggered = true;

        TriggerAction();
    }

    protected abstract void TriggerAction();
}