﻿using Assets.Scripts;
using UnityEngine;

public class Teleport : TriggerBase, IHasStepHandler
{
    public GameObject Destination;

    private bool _recharge = false;

    void Start()
    {
        GameManager.instance.AddStepHandler(this);
    }

    protected override void TriggerAction()
    {
        var player = GameObject.FindGameObjectWithTag("Player");

        var destinationTeleport = Destination.GetComponent<Teleport>();

        if (destinationTeleport != null)
            destinationTeleport.IsActive = false;

        player.GetComponent<Player>()
            .AfterMoving(() => player.transform.position = Destination.transform.position);
    }

    public void BeforeMove()
    {
        _recharge = !IsActive && IsRepeatable;
    }

    public void AfterMove()
    {
        if (_recharge)
            IsActive = true;
    }
}