﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using UnityEngine;

public class Trigger : TriggerBase
{
    public GameObject[] TriggeredObjects;

    private IEnumerable<IHasTrigger> _triggeredObjects;

    void Start()
    {
        _triggeredObjects = TriggeredObjects.Select(o => o.GetComponent<IHasTrigger>());
    }

    protected override void TriggerAction()
        => _triggeredObjects.Apply(e => e.OnTrigger());
}