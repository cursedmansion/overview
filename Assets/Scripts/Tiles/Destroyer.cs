﻿using UnityEngine;

public class Destroyer : TriggerBase
{
    public GameObject[] Targets;
    
    void Start()
    {
    }

    protected override void TriggerAction()
        => Targets.Apply(t => t.SetActive(false));
}