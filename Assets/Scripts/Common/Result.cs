﻿public class Result<TResult>
{
    public bool IsSuccess { get; }
    public bool IsFailed => !IsSuccess;
    
    public TResult Value { get; }

    private Result(TResult value) : this(true)
    {
        Value = value;
    }

    private Result(bool isSuccess)
    {
        IsSuccess = isSuccess;
    }

    public static Result<TResult> Success(TResult result)
        => new Result<TResult>(result);

    public static Result<TResult> Fail()
        => new Result<TResult>(false);
}

