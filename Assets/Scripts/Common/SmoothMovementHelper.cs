﻿using System;
using System.Collections;
using System.Threading;
using Assets.Scripts;
using UnityEngine;

public static class SmoothMovementHelper
{
    public static void Move<T>(T item,
        Vector3 destination,
        Action onStart,
        Action onEnd) where T : MonoBehaviour, IMovingObject 
        => item.StartCoroutine(MoveInternal(item, destination, onStart, onEnd));

    private static IEnumerator MoveInternal<T>(T item,
        Vector3 destination,
        Action onStart,
        Action onEnd)
        where T : MonoBehaviour, IMovingObject
    { 
        onStart();

        var startPosition = item.transform.position;
        var t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime * item.SpeedValue;
            item.transform.position = Vector3.Lerp(startPosition, destination, t);
            yield return null;
        }

        onEnd();

        yield return 0;
    }
}