﻿public static class Tags
{
    public const string Player = nameof(Player);

    public const string Enemy = nameof(Enemy);

    public const string Wall = nameof(Wall);
}