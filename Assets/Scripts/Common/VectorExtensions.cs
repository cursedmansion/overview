﻿using UnityEngine;

public static class VectorExtensions
{
    public static Vector3 As3D(this Vector2 vector)
        => new Vector3(vector.x, vector.y, 0);
}